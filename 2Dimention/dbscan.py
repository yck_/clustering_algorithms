from sklearn.cluster import DBSCAN  
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn.neighbors import NearestNeighbors
from sklearn.metrics import silhouette_score
import datetime

#read data
dataset= pd.read_csv('2dPoints.csv')
X = dataset.to_numpy()

#read clusters
dataset= pd.read_csv('2dClusters.csv')
Y = dataset.to_numpy()

##################################################################################
# # for optimal value of epsilon I calculated the nearest neighbour and
# # sorted the distances. The elbow point gave me the optimal epsilon value.

# neighbors = NearestNeighbors(n_neighbors=35)
# neighbors_fit = neighbors.fit(X)
# distances, indices = neighbors_fit.kneighbors(X)

# distances = np.sort(distances, axis=0)
# distances = distances[:,1]
# plt.plot(distances)
# plt.show()

##################################################################################

#apply dbscan for different cluster numbers and generate Silhouette Score
for i in [780,800,850,900,700,650,600,550,690,695]:
    t1=datetime.datetime.now()
    db_model=DBSCAN(eps=i,min_samples=5,n_jobs=-1)
    db_model.fit(X)
    t2=datetime.datetime.now()

    #for silhoulette cofficient
    labelsCoeff = db_model.labels_
    print(f'Silhouette Score(epsilon value={i}): {silhouette_score(X, labelsCoeff)} computational time:{t2-t1}')

print("=================================================================")
#apply dbscan for different cluster numbers and generate Silhouette Score
for i in [2,3,4,5,6,7,8,9,10,11,12,13,14,15]:
    t1=datetime.datetime.now()
    db_model=DBSCAN(eps=800,min_samples=i,n_jobs=-1)
    db_model.fit(X)
    t2=datetime.datetime.now()

    #for silhoulette cofficient
    labelsCoeff = db_model.labels_
    print(f'Silhouette Score(min_samples={i}): {silhouette_score(X, labelsCoeff)} computational time:{t2-t1}')


#generate results for most optimal values
t1=datetime.datetime.now()
db_model=DBSCAN(eps=800,min_samples=9,n_jobs=-1)
db_model.fit(X)
t2=datetime.datetime.now()

labels = db_model.labels_

#for silhoulette cofficient
labelsCoeff = db_model.labels_
print(f'Silhouette Score(eps=800,min_samples=9): {silhouette_score(X, labelsCoeff)} computational time:{t2-t1}')

#this process made for better visualization
labelMap=[5,31,2,19,9,7,15,1,27,6,32,3,12,4,24,34,0,10,25,30,16,33,11,28,22,17,13,20,8,26,14,18,23,29,21,36,38,37]
for i in range(len(labels)):
    if(labels[i]!=-1):
        labels[i]=labelMap[int(labels[i])-1]

#plotting part the identified clusters and compare
fig, axes = plt.subplots(1, 2, figsize=(12,7))
axes[0].scatter(X[:, 0], X[:, 1], c=Y, cmap='gist_rainbow', edgecolor='k', s=100)
axes[1].scatter(X[:, 0], X[:, 1], c=labels, cmap='jet', edgecolor='k', s=100)
axes[0].set_xlabel('x')
axes[0].set_ylabel('y')
axes[1].set_xlabel('x')
axes[1].set_ylabel('y')
axes[0].set_title('Truth')
axes[1].set_title('Predicted')
plt.show()
