import matplotlib.pyplot as plt
import pandas as pd
from sklearn.cluster import KMeans
import numpy as np
from sklearn.metrics import silhouette_score
import datetime


def main():
    #read data
    dataset= pd.read_csv('2dPoints.csv')
    X = dataset.to_numpy()

    #read clusters
    dataset= pd.read_csv('2dClusters.csv')
    Y = dataset.to_numpy()

    #apply kMeans, cluster count is set to 35
    # 35 is taken from the datasets website.
    t1=datetime.datetime.now()
    kmean = KMeans(n_clusters = 35, random_state=1)
    kmean.fit(X)
    t2=datetime.datetime.now()

    #for silhoulette cofficient
    label=kmean.predict(X)
    print(f'Silhouette Score(number of clusters=35): {silhouette_score(X, label)} computational time:{t2-t1}')

    #labels
    labels = kmean.labels_

    #apply kMeans for different cluster numbers and generate Silhouette Score for different values
    for i in [5,10,15,20,25,30,40,45]:
        t1=datetime.datetime.now()
        kmean = KMeans(n_clusters = i, random_state=1)
        kmean.fit(X)
        t2=datetime.datetime.now()

        #for silhoulette cofficient
        label=kmean.predict(X)
        print(f'Silhouette Score(number of clusters={i}): {silhouette_score(X, label)} computational time:{t2-t1}')

    # Plot the identified clusters and compare
    fig, axes = plt.subplots(1, 2, figsize=(12,7))
    axes[0].scatter(X[:, 0], X[:, 1], c=Y, cmap='gist_rainbow', edgecolor='k', s=150)
    axes[1].scatter(X[:, 0], X[:, 1], c=labels, cmap='jet', edgecolor='k', s=150)
    axes[0].set_xlabel('x')
    axes[0].set_ylabel('y')
    axes[1].set_xlabel('x')
    axes[1].set_ylabel('y')
    axes[0].set_title('Truth')
    axes[1].set_title('Predicted')
    plt.show()




if __name__ == "__main__":             
    main()


