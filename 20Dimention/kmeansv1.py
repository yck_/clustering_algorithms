from sklearn.cluster import DBSCAN 
import datetime
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score


def main():
    #read data
    dataset1= pd.read_csv('mushrooms.csv')

    with open('mushrooms.csv') as f:
        first_line = f.readline()
        columnNames=first_line[:-1].split(',')

    #convert data rows from string to integer
    for i in columnNames:
        unique_vals = dataset1[i].unique()
        dataset1[i].replace(to_replace=unique_vals,
                value= list(range(len(unique_vals))),
                inplace=True)

    X = dataset1.to_numpy()

    #apply kMeans for different cluster numbers and generate Silhouette Score
    for i in [2,3,4,5,6,7,8,9,10,15,20,25,30,40,45]:
        t1=datetime.datetime.now()
        kmean = KMeans(n_clusters = i, random_state=1)
        kmean.fit(X)
        t2=datetime.datetime.now()

        #for silhoulette cofficient
        label=kmean.predict(X)
        print(f'Silhouette Score(number of clusters={i}): {silhouette_score(X, label)} computational time:{t2-t1}')

if __name__ == "__main__":             
    main()



