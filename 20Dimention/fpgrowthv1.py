import pandas as pd
from mlxtend.preprocessing import TransactionEncoder
import pyfpgrowth
import datetime

#read data
dataset1= pd.read_csv('mushrooms.csv')
X = dataset1.to_numpy()

t1=datetime.datetime.now()

# 2 is the support level. 
# It finds the minimum 2 support items
freqPatterns = pyfpgrowth.find_frequent_patterns(X, 800)
fpRules = pyfpgrowth.generate_association_rules(freqPatterns, 0.7)
t2=datetime.datetime.now()

print("Patterns")
print(freqPatterns)
print("====================")
print("Rules")
print(fpRules)
print(f"Computational Time:{t2-t1}")
