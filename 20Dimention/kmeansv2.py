import matplotlib.pyplot as plt
import pandas as pd
from sklearn.cluster import KMeans
import numpy as np
from sklearn.metrics import silhouette_score
import datetime

def main():
    #read data
    dataset= pd.read_csv('southgermancredit.csv')
    X = dataset.to_numpy()


    #apply kMeans for different cluster numbers and generate Silhouette Score
    for i in [2,3,4,5,8,9,10,15,20,25,30,35,40,45]:
        t1=datetime.datetime.now()
        kmean = KMeans(n_clusters = i, random_state=1)
        kmean.fit(X)
        t2=datetime.datetime.now()

        #for silhoulette cofficient
        label=kmean.predict(X)
        print(f'Silhouette Score(number of clusters={i}): {silhouette_score(X, label)} computational time:{t2-t1}')






if __name__ == "__main__":             
    main()


