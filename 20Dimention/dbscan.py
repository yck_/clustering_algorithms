from sklearn.cluster import DBSCAN  
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn.neighbors import NearestNeighbors
from sklearn.metrics import silhouette_score
import datetime

#read data
dataset= pd.read_csv('southgermancredit.csv')
X = dataset.to_numpy()

##################################################################################
# for optimal value of epsilon I calculated the nearest neighbour and
# sorted the distances. The elbow point gave me the optimal epsilon value.

# neighbors = NearestNeighbors(n_neighbors=20)
# neighbors_fit = neighbors.fit(X)
# distances, indices = neighbors_fit.kneighbors(X)

# distances = np.sort(distances, axis=0)
# distances = distances[:,1]
# plt.plot(distances)
# plt.show()

##################################################################################

#apply dbscan for different cluster numbers and generate Silhouette Score
for i in range(90,221,10):
    t1=datetime.datetime.now()
    db_model=DBSCAN(eps=i,min_samples=5,n_jobs=-1)
    db_model.fit(X)
    t2=datetime.datetime.now()

    #for silhoulette cofficient
    labelsCoeff = db_model.labels_
    print(f'Silhouette Score(epsilon value={i},min_samples=5): {silhouette_score(X, labelsCoeff)} computational time:{t2-t1}')

print("=================================================================")
#apply dbscan for different cluster numbers and generate Silhouette Score
for i in [2,3,4,5,6,7,8,9,10,11,12,13,14,15]:
    t1=datetime.datetime.now()
    db_model=DBSCAN(eps=220,min_samples=i,n_jobs=-1)
    db_model.fit(X)
    t2=datetime.datetime.now()

    #for silhoulette cofficient
    labelsCoeff = db_model.labels_
    print(f'Silhouette Score(epsilon value=220,min_samples={i}): {silhouette_score(X, labelsCoeff)} computational time:{t2-t1}')

