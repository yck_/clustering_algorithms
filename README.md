# Clustering Algorithms
Gebze Technical University 2021 Data Mining Course Homweork

This reporisotry implemenmted for Data Mining course homework. Homework description:




Prepare a report that presents the results for the questions mentioned below using two datasets, where DS1 has two dimentions and DS2 has at least 20 dimentions. You have to find the datasets yourself.
	
Find clusters using Frequent Pattern Growth, k-means, DB scan and Chameleon clustering techniques. You may use data mining tools to find clusters. 
	
Present the clusters of DS1 for each technique using graphics.
	
Calculate silhouette coefficient for each clustering technique. Compare and interpret the silhouette score with the extracted clusters. 
	
Present computational time and time complexity of each clustering model.
	
Which clustering technique is more suitable for your dataset? Write a discussion about it using the results mentioned above and characteristics of the clusters and the dataset.
